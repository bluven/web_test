
class CommonActionMixin(object):

    def logout(self):
        '''
            Logout by clicking on 'Logout' button.(Recommand)
        '''

        self.driver.find_element_by_xpath("//a[text()='Logout']").click()

    def safely_logout(self):
        '''
            Logout out by sending a url
        '''
        self.go('logout/')

    def close(self):
        self.driver.quit()




