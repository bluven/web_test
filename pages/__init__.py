
from login_page import LoginPage
from main_page import MainPage
from user_page import UserPage

__all__ = [LoginPage, MainPage, UserPage]
