class BasePage(object):

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(5)
        self.verification_errors = []

        self.base_url = 'http://192.168.1.101:8000/%s'

    def go(self, url):
        self.driver.get(self.base_url %url)

    def get_url(self, url):
        return self.base_url + url

    def close(self):
        self.driver.quit()
