import time
from selenium.webdriver.common.action_chains import ActionChains

from base_page import BasePage
from mixins import CommonActionMixin

'''
    This page implement User Manage functions
'''

class UserPage(BasePage, CommonActionMixin):

    def __init__(self, driver):
        super(UserPage, self).__init__(driver)
        self.__window = self.driver.window_handles[0]
    
    @property
    def title(self):
        return self.driver.title

    @property
    def edit_page(self):
        return EditPage(self.driver)

class EditPage(BasePage):

    def __init__(self, driver):

        self.__window = driver.window_handles[0]
        driver.switch_to_frame(1)
        self.driver = driver

    def input_username(self, username):
        
        self.__input_field(self.username, username)

    def clear_and_input_username(self, username):

        self.username.clear()
        self.__input_field(self.username, username)

    def input_email(self, email):
        
        self.__input_field(self.email, email)

    def clear_and_input_email(self, email):

        self.email.clear()
        self.__input_field(self.email, email)

    def clear_and_input_password(self, password):
        
        self.password.clear()
        self.__input_field(self.password, password)

    def input_password(self, password):
        self.__input_field(self.password, password)

    def __input_field(self, element, value, clear=True):

        if clear:
            element.clear()

        chain = ActionChains(self.driver)
        chain.move_to_element(element).click()\
                .send_keys(value).move_by_offset(-100, 0)\
                .click().perform()

    @property
    def username_hint(self):
        return self.driver.find_element_by_xpath("//input[@name='username']/../p").text

    @property
    def password_hint(self):
        return self.__get_hint('password')
        
    @property
    def email_hint(self):
        return self.__get_hint('email')

    def __get_hint(self, field_name):
        time.sleep(0.5) 
        return self.driver.find_element_by_xpath("//input[@name='%s']/../p" %field_name).text

    @property
    def username(self):
        return self.driver.find_element_by_name('username')

    @property
    def password(self):
        return self.driver.find_element_by_name('password')

    @property
    def email(self):
        return self.driver.find_element_by_name("email")
