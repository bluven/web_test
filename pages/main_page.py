
from base_page import BasePage
from mixins import CommonActionMixin
from user_page import UserPage

'''
    This page implement Main Page functions
'''


class MainPage(BasePage, CommonActionMixin):

    def get_user_page(self):
       self.__click_menu('mnuMainAdmin', 'mnuUser_text')
       return UserPage(self.driver)

    def __click_menu(self, menu_id, item_id):
        self.driver.find_element_by_id(menu_id).click()
        self.driver.find_element_by_id(item_id).click()

    def is_main_menu_available(self):
        try:
            self.driver.find_element_by_id("mnuMain")
            return True
        except:
            return False

