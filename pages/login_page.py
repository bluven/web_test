
from base_page import BasePage
from main_page import MainPage
'''
    This page implement login functions
'''

class LoginPage(BasePage):

    def login(self, username, password):

        self.go('login/')
        
        self.driver.find_element_by_id("username").clear()
        self.driver.find_element_by_id("username").send_keys(username)

        self.driver.find_element_by_id("password").clear()
        self.driver.find_element_by_id("password").send_keys(password)

        self.driver.find_element_by_name("login").click()

        return MainPage(self.driver)

    def logout(self):
        '''
            Logout by clicking on 'Logout' button.(Recommand)
        '''

        self.driver.find_element_by_xpath("//a[text()='Logout']").click()

    def safely_logout(self):
        '''
            Logout out by sending a url
        '''

        self.go('logout/')

    def close(self):

        self.driver.quit()

    def is_main_menu_available(self):
        try:
            self.driver.find_element_by_id("mnuMain")
            return True
        except:
            return False

    def get_error_message(self):
        
        return self.driver.find_element_by_xpath("//td/p").text

