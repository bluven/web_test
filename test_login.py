from selenium import webdriver
import unittest

from pages import LoginPage

class TestLogin(unittest.TestCase):

    def setUp(self):
        self.login_page = LoginPage(webdriver.Firefox())

    def test_login(self):

        main_page = self.login_page.login('admin', 'admin')
        self.assertEqual(True, main_page.is_main_menu_available(), 'Login failed')
        main_page.logout()

    def test_wrong_username_or_password(self):
        self.login_page.login('admin2', 'admin')
        self.assertEqual('Incorrect user name or password, please try again.',
                       self.login_page.get_error_message()) 

    def test_user_already_logined(self):
        self.login_page.login('admin', 'admin')
        self.login_page.login('admin', 'admin')

    def tearDown(self):

        self.login_page.safely_logout()
        self.login_page.close()

if __name__ == '__main__':
    unittest.main()
