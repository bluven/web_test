from selenium import webdriver
import unittest

from pages import LoginPage

class TestMain(unittest.TestCase):

    def setUp(self):
        login_page = LoginPage(webdriver.Firefox())
        self.main_page = login_page.login('admin', 'admin')

    def test_user_page(self):
        user_page = self.main_page.get_user_page()
        
        right_title = u'Manage Users'
        self.assertEqual(right_title, user_page.title, 
                'User Page titile is suppose to be "%s", not "%s"' \
                %(right_title, user_page.driver.title))

    def tearDown(self):

        self.main_page.safely_logout()
        self.main_page.close()

if __name__ == '__main__':
    unittest.main()
