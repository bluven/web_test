from selenium import webdriver
import unittest

from pages import LoginPage

class TestUser(unittest.TestCase):

    def setUp(self):
        login_page = LoginPage(webdriver.Firefox())
        main_page = login_page.login('admin', 'admin')
        self.user_page = main_page.get_user_page()

    def test_username(self):
        edit_page = self.user_page.edit_page 

        edit_page.input_username('test')
        self.assertEqual('Ok!', edit_page.username_hint)

        edit_page.clear_and_input_username('a')
        self.assertEqual('Ensure this value has at least 3 characters (it has 1).',
                        edit_page.username_hint)

        edit_page.clear_and_input_username('a' * 17)
        self.assertEqual(16, len(edit_page.username.get_attribute('value')))

        edit_page.clear_and_input_username('admin')
        self.assertEqual('This name is being used by other user.', 
                        edit_page.username_hint)

    def test_email(self):

        edit_page = self.user_page.edit_page 

        edit_page.input_email('test@test.com')
        self.assertEqual('Ok!', edit_page.email_hint)

        edit_page.clear_and_input_email('test')
        self.assertEqual('An email address must contain a single @', 
                        edit_page.email_hint)

        edit_page.clear_and_input_email('test@test')
        self.assertEqual('The domain portion of the email address'
                         ' is invalid (the portion after the @: test', 
                        edit_page.email_hint)

        edit_page.clear_and_input_email('admin@example.com')
        self.assertEqual('This email address is being userd by other user.',
                        edit_page.email_hint)

    def test_password(self):

        edit_page = self.user_page.edit_page

        edit_page.input_password('admin')
        self.assertEqual('Ok!', edit_page.password_hint)

        edit_page.clear_and_input_password('admin')
        self.assertEqual('Ok!', edit_page.password_hint)

    def tearDown(self):
        self.user_page.safely_logout()
        self.user_page.close()

if __name__ == '__main__':
    unittest.main()
